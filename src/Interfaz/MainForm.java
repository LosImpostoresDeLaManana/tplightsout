package Interfaz;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class MainForm {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainForm window = new MainForm();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainForm() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */

	private void cambiarColor(JButton boton) {
		if (boton.getBackground() == Color.BLACK) {
			
			boton.setBackground(Color.WHITE);
		} else {
			boton.setBackground(Color.BLACK);
		}
		//agregué esta línea para probar , despues la sacamos. Funciona bien la pos en la matriz
		//pero creo que el tema del random, y el valor de cada boton debería generarse en la clase juego.
		boton.setLabel(boton.getName());
		
	}

	private JPanel generarPanel(int columna, int fila) {
		// Un panel nuevo donde almacenar los botones
		JPanel panelNuevo = new JPanel();
		panelNuevo.setBounds(100, 100, 603, 410);
		panelNuevo.setLayout(new GridLayout(columna, fila, 1, 1));
		JButton[][] matrizBotones = new JButton[columna][fila];
		// Recorremos segun cantidad de columnas
		for (int i = 0; i < columna; i++) {
			// Recorremos segun cantidad de filas
			for (int j = 0; j < fila; j++) {
				// Creamos un nuevo boton (Ejemplo : (1,1))
				JButton botonGenerado = new JButton(" ");
				botonGenerado.setName(" "+i+j);
				int random = (int) (Math.random() * 3);
				// Si se da la chance aleatoria le cambiamos el color a blanco
				if (random > 1) {
					botonGenerado.setBackground(Color.WHITE);
				} else {
					// Le damos un color de fondo negro
					botonGenerado.setBackground(Color.BLACK);
				}
				matrizBotones[i][j] = botonGenerado;
				botonGenerado.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						cambiarColor(botonGenerado);
					}
				});// Agregamos el bot�n al panel
				panelNuevo.add(botonGenerado);
			}
		}
		// Devolvemos el panel lleno de botones
		return panelNuevo;
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 603, 410);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		// PANTALLA 4X4
		JButton btnNewButton = new JButton("4x4");
		btnNewButton.setBounds(49, 305, 49, 23);
		frame.getContentPane().add(btnNewButton);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				btnNewButton.setVisible(false);
				frame.setContentPane(generarPanel(4, 4));
			}
		});

		// PANTALLA 5X5
		JButton btnNewButton_1 = new JButton("5x5");
		btnNewButton_1.setBounds(263, 305, 49, 23);
		frame.getContentPane().add(btnNewButton_1);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnNewButton_1.setVisible(false);
				frame.setContentPane(generarPanel(5, 5));
			}
		});

		// PANTALLA 6X6
		JButton btnNewButton_2 = new JButton("6x6");
		btnNewButton_2.setBounds(450, 305, 49, 23);
		frame.getContentPane().add(btnNewButton_2);
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnNewButton_2.setVisible(false);
				frame.setContentPane(generarPanel(6, 6));
			}
		});
	}
}
