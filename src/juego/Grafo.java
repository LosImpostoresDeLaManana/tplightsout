package juego;
import java.util.HashSet;
import java.util.Set;

public class Grafo {

	private boolean[][] A; // matriz de adyacencia

	public Grafo(int vertices) {

		A = new boolean[vertices][vertices]; // inicializamos con todos false
	}
	public boolean verValor(int x, int y) {
		return A[x][y];
	}

	public int tamano() {
		return A.length;
	}
	//vecinos de un vertice . en nuestro caso el vértice representa la luz del tablero
	public Set<Integer> vecinos(int i){
		verificarVertice(i);
		Set<Integer> ret = new HashSet<Integer>();
		for(int j = 0;j<this.tamano();j++) {
			if(this.existeArista(i, j)) {
				ret.add(j);
			}
			
		}
		return ret;
	}
	
	
	public void agregarArista(int i, int j) {

		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);

		A[i][j] = true;
		A[j][i] = true;

	}

	public boolean existeArista(int i, int j) {

		verificarVertice(i);
		verificarVertice(j);

		return A[i][j];
	}

	public void eliminarArista(int i, int j) {

		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);

		A[i][j] = false;
		A[j][i] = false;

	}

	private void verificarDistintos(int i, int j) {
		if (i == j) {
			throw new IllegalArgumentException("No debe haber loops: " + "(" + i + ", " + j + ")");

		}
	}

	private void verificarVertice(int i) {
		if (i < 0) {
			throw new IllegalArgumentException("el vertice no puede ser negativo");
		}

		if (i >= A.length) {
			throw new IllegalArgumentException("Los vertices deben estar entre 0 y " + A.length);
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(int i=0;i<this.tamano();i++) {
			for(int j=0;j<A[0].length;j++) {
				sb.append("["+i+","+j+"]");
			}
		}
		return sb.toString();
	}
	
	

	
}
