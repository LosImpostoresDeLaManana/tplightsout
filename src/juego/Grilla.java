package juego;

import java.util.ArrayList;

public class Grilla {//implemen
	
	private boolean [][]tablero;
	private int record;
	private int turnos;
	
	public Grilla(int n) {
		tablero = new boolean[n][n];
		record=0;
		turnos=100;
	}
	public void cambiarValorCasillas(int i, int j) { //a la ingresada y a sus vecinos
		tablero[i][j]=!tablero[i][j];
	}
	
	public void actualizarTurnos() {
		turnos-- ;
	}
	
	public boolean todosSonPrendidos() {//true prendidas, y false apagadas???
		boolean estanTodasPrendidas=true;
		for(int i=0;i<tablero.length;i++) {
			for(int j=0;j<tablero.length;j++) {
				estanTodasPrendidas=estanTodasPrendidas && estanPrendidas(i,j);	
			}			
		}
		return false;
	}
	
	private boolean estanPrendidas(int i, int j) {
		return tablero[i][j];
	}
	
	public boolean seQuedoSinTurnos() {
		if(turnos==0)
			return true;
		return false;
	}
	
	public ArrayList<Boolean> estadoTablero(){
		ArrayList<Boolean>lista = new ArrayList<>();
		for(int i=0;i<tablero.length;i++) {
			for(int j=0;j<tablero[0].length;j++) {
				
				lista.add(tablero[i][j]);
				
			}
		}
		return lista;
	}
	
	public boolean devolverValorCasilla(int i, int j) {
		return tablero[i][j];
	}
	

}
