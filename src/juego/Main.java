package juego;

public class Main {

	public static void main(String[] args) {
		
		
		Grafo grilla = new Grafo(16);
		grilla.agregarArista(0, 1);
		grilla.agregarArista(1, 2);
		grilla.agregarArista(2, 3);
		grilla.agregarArista(4, 5);
		grilla.agregarArista(5, 6);
		grilla.agregarArista(6, 7);
		grilla.agregarArista(8, 9);
		grilla.agregarArista(9, 10);
		grilla.agregarArista(10, 11);
		grilla.agregarArista(12, 13);
		grilla.agregarArista(13, 14);
		grilla.agregarArista(14, 15);
		grilla.agregarArista(0, 4);
		grilla.agregarArista(1, 5);
		grilla.agregarArista(2, 6);
		grilla.agregarArista(3, 7);
		grilla.agregarArista(4, 8);
		grilla.agregarArista(5, 9);
		grilla.agregarArista(6, 10);
		grilla.agregarArista(7, 11);
		grilla.agregarArista(8, 12);
		grilla.agregarArista(9, 13);
		grilla.agregarArista(10,14);
		grilla.agregarArista(11,15);

		
		System.out.println(grilla.toString());
	
		System.out.println(grilla.verValor(0, 0));
		
		//con el metodo vecinos de grafo, tenemos las luces vecinas de la luz pasada por parametro.
		//tendríamos que devolverlas al controlador para que este actualice la interfaz
		System.out.println(grilla.vecinos(1));
	}

}
